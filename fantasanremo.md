# Fantasanremo


## Canzone
- **Canzone:**
- **Simili:**
- **Testo:**
- **Opinione:**
- **Voto:**
- **Fantasanremo:**

## Morandi - Apri tutte le porte | Jovanotti
- **Canzone:** normale
- **Simili:** il grande babumba
- **Testo:** normale
- **Opinione:** potrebbe andare bene
- **Voti:** 6
- **Fantasanremo:** Non e' molto potente

## Giusy Ferreri - Miele | Takagi ketra
- **Canzone:** non è una canzone commerciale quindi ne carne ne pesce
- **Simili:**
- **Testo:** nella media
- **Opinione:** forse ha un album e vuole cambiare un po' andando a Sanremo
- **Voto:** 6
- **Fantasanremo:** potrebbe portare qualcosa sul palco

## Irama - Ovunque sarai
- **Canzone:** ballad minimale diversa dal solito. Non fortissima, rischio Aiello trash
- **Simili:** Simile ad Aiello
- **Testo:** non e' originale forse scontato. Non e' come il vecchio Irama
- **Opinione:** forse ha avuto il contentino per non aver partecipato alla scorsa edizione
- **Voto:** 6
- **Fantasanremo:** potrebbe essere normale

## Mattero Romano - Virale | Dardust
- **Canzone:** non e' una ballad ma non è malaccio. E' più radiofonica e dipende da come la canta
- **Simili:** 
- **Testo:**
- **Opinione:** fascia di pubblico difficile da prendere, viene da TikTok. Parte dal festival e è giovane deve poter sfruttare il festival. Dipende a che ora c'è in serata. 50/50
- **Voto:** 6
- **Fantasanremo:** potrebbe dare soddisfazioni con i bonus

## Yuman - Ora e qui | 
- **Canzone:** soul molto difficile come genere da cantare e scrivere. Quindi passa o canzone o testo. Potrebbe essere una cosa come Musica Leggerissima.
- **Simili:** 
- **Testo:** 
- **Opinione:** ha vinto sanremo giovani lo scorso anno
- **Voto:** 6
- **Fantasanremo:** è giovane e potrebbe anche lui fare il fantasanremo ma è molto serio

## Aka 7even - Perfetta così | impronunciabili
- **Canzone:** a tempo in stile ci sta
- **Simili:** come le sue altre
- **Testo:** parla di accettazione di se e è un bel messaggio.
- **Opinione:** una bella canzone che può funzionare, ma ci possono essere canzoni simili. Arriva da amici. Aka7 e Sangiovanni è la finale di Amici. Aka 7 potrebbe essere apprezzato di più per la sua realtà, Sangiovanni è commerciale e meno umano, è costruito. Ha meno presa su un festival che su un talent. SARANNO ALTI IN CLASSIFICA
- **Voto:** 6.5
- **Fantasanremo:** errore di nome, Saranno alti in classifica

## **Highsnob e Hu - Abbi cura di te** | 
- **Canzone:** rappato e molto particolare e mai visto a Sanremo cose simili. Melodico, moderno e rappato. Artisti che funzionano insieme e e' qualcosa di nuovo.  
- **Simili:** 
- **Testo:**
- **Opinione:** non sono conosciuti molto.
- **Voto:** 6.5
- **Fantasanremo:** arrivano a metà

## Iva Zanicchi - Voglio amarti | Valli che ritorna
- **Canzone:** brano classico di 40 anni, blues non classico. Dal vivo potrebbe andare bene.
- **Simili:** Rita Pavone e non Orietta Berti
- **Testo:** 
- **Opinione:** forse l'hanno presa come comparsa e perchè copriva la nicchia boomer. Non è detto che faccia come Orietta Berti che è diventata ancora un po' famosa. 
- **Voto:** 6.5
- **Fantasanremo:** polemiche 

## Rkomi - Insuperabile
- **Canzone:** trap, hiphop e trap. Non si capisce se vuole fare pop o rap hip hop
- **Simili:** Maleducata di Achille Lauro, partire da te, taxi driver. 
- **Testo:** parla del successo e dell'olimpo come obiettivo
- **Opinione:** fedele a se stesso e porta se stesso. Si SOVRAPPONE A TANTI ALTRI GENERI. Potrebbe essere la prova di un nuovo pubblico fatta dai produttori.
- **Voto:** 6.5
- **Fantasanremo:** potrebbe arrivare non in cima ma ha tanto supporto e vuole aprirsi ad altro pubblico

## **Sangiovanni - Farfalle** | uno dei nomi più attesi
- **Canzone:** brano dance bello e pompa bene come hit. 
- **Simili:** le sue canzoni
- **Testo:** non c'è chissà quale impegno è giovane
- **Opinione:** può funzionare di più di Aka 7even e Rkomi. Quest'anno forse è avvantaggiato chi porta una ballad quindi lui meno?
- **Voto:** 6.5
- **Fantasanremo:** potrebbe pompare parecchio perchè giovane e diventare famoso

## Tananai - Sesso occasionale
- **Canzone:** brano attuale stile anni 60. Molto paraculo
- **Simili:**
- **Testo:**
- **Opinione:** giovan
- **Voto:**
- **Fantasanremo:**

## **Achille Lauro - Domenica**
- **Canzone:** rock classico stile Rolls Roice. Rimane in testa tantissimo, non è una novità. C'è un coro Ghospell che potrebbe stupire
- **Simili:** Rolls Roice
- **Testo:** Si cita da solo
- **Opinione:** Si autocita, e è un clichè ma è uguale a se stesso. Forse non c'è più Hype per Achille. Cambia sempre genere e riferimenti e non si capisce più chi è. Vuole stupire sicuro!
- **Voto:** 6.5
- **Fantasanremo:** Potrebbe dare tanti bonus dato che vuole stupire!

## **Giovanni Truppi - Tuo padre mia madre e Lucia**
- **Canzone:** Indie moltoooo originale. Brano recitato molto particolare! Brunori sas. Tosca come teatralità
- **Simili:**
- **Testo:** molto potente che potrebbe raccontare una storia molto forte. Dedica d'amore
- **Opinione:** potrebbe puntare al premio della critica!
- **Voto:** 6.5
- **Fantasanremo:** premio della critica se la gioca con Ranieri

## Le Virbrazioni - tantissimo | stessi autori di sempre
- **Canzone:** è un bel pezzo rock elettronico con tanti BPM. Rimane molto in testa e c'è Vessicchio 
- **Simili:** Così sbagliato
- **Testo:** normale
- **Opinione:** Passerà sicuramente in radio e è molto orecchiabile.
- **Voto:** 6.5
- **Fantasanremo:** Non so sinceramente. C'è Vessicchio possono esserci meme e roba figa e molta gente

## **Noemi - Ti amo non lo so dire | Mahmood**
- **Canzone:** non è glicine. Parte come ballad e diventa pop dance nel ritornello. Molto radiofonico e stupisce nel ritornello!
- **Simili:**
- **Testo:** scritto da Mahmood e si potrebbe sentire che è il suo
- **Opinione:** è molto radiofonico e potrebbe andare molto bene dato che è molto bipolare. Però non è che poi sembra che se la cantava Mahmood era meglio?!
- **Voto:** 7
- **Fantasanremo:** potrebbe essere molto buona e arrivare sul podio

## *Ana Mena - Duecentomilaore |*
- **Canzone:** bel pezzo a cassa dritta con fisarmonica in stile Argentino. Non è tormentone! 
- **Simili:** amami amami di Celentano
- **Testo:** ci porta in Sudamerica
- **Opinione:** il latino americano è stato ultra usato e abusato. si spera che possa essere all'altezza di qualcosa di nuovo, non è detto che sia il posto giusto dove presentarlo. Dicono che è molto buona. Ci si vede l'onestà in quello che ha fatto!
- **Voto:** 7.5
- **Fantasanremo:** potrebbe essere associata dal pubblico a Elettra Lamborghini. Fa come Gaia all'anno scorso

## **Dargen D'amico - dove si balla | Roberts (ha scritto Diodato 2 anni fa)**
- **Canzone:** Tormentone come Musica Leggerissima
- **Simili:** Battiato, Cochi renato, la dance di fine anni 90. Ricorda un sacco di cose 
- **Testo:** brano della vita molto figo e molto malinconico sulle mascherine. 
- **Opinione:** tormentone di questo Sanremo ma non vincerà. E' l'inno di questo Sanremo. SONO LA RAPPRESENTANTE DELL'ANNO SCORSO!
- **Voto:** 7.5
- **Fantasanremo:** Potrebbe fare il podio insieme alla rappresentante

## *Donatella Vettore Ditonellapiaga | Chimica*
- **Canzone:** è la quota trash, Ska Dance. Ritornello banale ma rimane in testa. 
- **Simili:** 
- **Testo:** se la prende con le suore
- **Opinione:** potrebbe essere una canzone trash ma poi va meglio
- **Voto:** 7.5
- **Fantasanremo:** boooh

## Emma Marrone - Ogni volta è così | dirige Michelin (proprio perchè donna?!)
- **Canzone:** un bel pezzo, sguaiato alla Mina. Effetto alla Aiello quando urla.
- **Simili:** 
- **Testo:** 
- **Opinione:** potrebbe passare inosservato. 
- **Voto:** 7.5
- **Fantasanremo:** passerà inosservato

## Massimo Ranieri - Lettera al di là del mare | Fabio E l'Acqua (Occidentalis)
- **Canzone:** un bel pezzo bellissimo teatrale sembra una romanza. Applausi
- **Simili:** evoca Dalla, Bocelli
- **Testo:** parla di emigrazione e è accostato al Titanic. In verità parla di emigrazione verso l'america inizi 900
- **Opinione:** ha fatto Occidentalis Karma.
- **Voto:** 7.5
- **Fantasanremo:** booh

## Michele Bravi - Inverno dei fiori
- **Canzone:** pezzo molto bello toccante. Potrebbe rendere molto dal vivo
- **Simili:**
- **Testo:** il testo molto interessante e maturo e molto profondo
- **Opinione:** molto profondo e introspettivo e ha avuto un periodo molto difficile. Ci aspettiamo di piangere. C'è molto hypeee su twitter
- **Voto:** 8
- **Fantasanremo:** boh non si sa

## *Elisa - O forse sei tu*
- **Canzone:** si accende dal primo ritornello, roba inusuale che cresce di continuo. Non ha molto impatto. Parte dopo 1.20m, potrebbe essere un contro o pro dato che tutti sono obbligati ad ascoltare.
- **Simili:** 
- **Testo:**
- **Opinione:** forse il crescendo potrebbe essere apprezzato e arrivare in alto nella classifica. Vuole mettersi in gioco e fare quello che vuole, non le puoi dire niente. Va per un fine promozionale ok ma anche perché le va di andare con qualcosa di serio, segno di rispetto. LEI PUO' MOSTRARSI COM'è perché non è commerciale e vuole fare vedere chi è. NB è una vecchia canzone che si è trovata in un cassetto quindi è di stampo vecchio?
- **Voto:** 8
- **Fantasanremo:** a partire dall'opinione

## Fabrizio Moro - sei tu
- **Canzone:** ballad di Sanremo. Pezzo d'amore molto bello in genere universale.
- **Simili:**
- **Testo:**
- **Opinione:** quando sceglie il festival non lo fa a caso. Va sempre consapevole di fare bene. Ti porta a casa il risultato. Ha un pubblico molto ancorato a se quindi non fa breccia su altri.
- **Voto:** 8
- **Fantasanremo:** non arriva sul podio

## **Mahmood e Blanco - Brividi | Team dietro molto potente**
- **Canzone:** ballad crescendo, non ha ritmica. Non è il brano che vince a mani basse.
- **Simili:** blue celeste
- **Testo:**
- **Opinione:** vanno bene per il pubblico e basta, ma forse sono troppo chiamati come risultato. Blanco è un Tha Suprime venuto bene. Sono quelli con le spalle più coperte sotto il punto di vista tecnico e pratico. Forse sulla carta ammortizzano i rischi, ne escono solo bene da Sanremo
- **Voto:** 8
- **Fantasanremo:**

## **La Rappresentante di Lista - Ciao Ciao**
- **Canzone:** pezzo funziona tantissimo, travolgente come lo scorso. La canzone funziona il ritornello è una bomba e spacca il culo. Cassa in 4 e tanta roba 
- **Simili:** mattia bazar
- **Testo:** tanta roba e fa riflettere un sacco
- **Opinione:** molto buona e è la seconda volta che vanno e potrebbero spaccare per bene
- **Voto:** 8.5
- **Fantasanremo:** Stupiranno con vestiti e altre cose, wow

# Classifica secondo i tizi
- La Rappresentante di Lista
- Mahmood
- Elisa

# Opinioni
## Youtube Arcuri
- **Sangionvanni:** ultimo ma ha venduto quanto RKomi e più dei Maneskin nel 2021!
- **Rkomi:** 12 apprezzato un sacco dal pubblico, arrangiato molto bene
- **Achille:** 11 si butta o vince o niente
- **Emma:** 10 forse non è fortissima
- **Morandi:** 9 gli ha scritto Jovanotti e ha fatto cagare in passato
- **Mahmood:** 8 si vede che è fatto a tavolino e sbombano
- **Moro:** 7 potrebbe finire in alto
- **Giovanni Truppi:** 6 il voto del pubblico possono seccarlo
- **Ditonellapiaga:** 5 forse giornalisti entusiasti
- **Ranieri:** 4 testo molto impegnato VINCE?
- **Dargen D'amico:** 3 scritto molto bene e spacca
- **Elisa:** 2 è obbligata a vincere!
- **La Rappresentante:** 1 spacca tutto!

# Classifica temporanea
- La Rappresentante di Lista  - 19
- Mahmood e Blanco - 35
- Dargen D'amico - 15
- Giovanni Truppi - 16
- Ditonellapiaga  - 15

- La Rappresentante di Lista  - 19
- Elisa - 28
- Dargen D'amico - 15
- Giovanni Truppi - 16
- Sangiovanni - 26

